package ictgradschool.industry.abstraction.farmmanager.animals;

public class Dog extends Animal {
    private final int MAX_VALUE = 1200;
    public Dog(){
        value = 600;
    }
    public void feed(){
        if (value<MAX_VALUE){
            value = value +(MAX_VALUE-value)/2;
        }
    }
    public int costToFeed (){
        return 50;
    }
    public String getType(){
        return "Dog";
    }
    public String toString() {

        return getType() + " - $" + value;
    }
}
