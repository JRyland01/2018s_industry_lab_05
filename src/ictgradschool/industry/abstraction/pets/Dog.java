package ictgradschool.industry.abstraction.pets;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal {
    @Override
    public String sayHello() {
        String dogGreeting = "woof woof";
        return dogGreeting;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        String myNameIs ="Bruno the dog";
        return myNameIs;
    }

    @Override
    public int legCount() {
        int dogLegs = 4;
        return dogLegs;
    }
}

