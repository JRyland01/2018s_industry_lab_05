package ictgradschool.industry.abstraction.pets;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    @Override
    public String sayHello() {
        String birdGreeting = "tweet tweet";
        return birdGreeting;
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        String myNameIs ="Tweety the bird";
        return myNameIs;
    }

    @Override
    public int legCount() {
        int birdLegs = 2;
        return birdLegs;
    }
}
