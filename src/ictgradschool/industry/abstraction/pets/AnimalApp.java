package ictgradschool.industry.abstraction.pets;

/**
 * Main program.
 */
public class AnimalApp {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the pets array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        for (int i = 0 ; i<list.length;i++){
            String name = list[i].myName();
            String sound =list[i].sayHello();
            System.out.println(name + " says " + sound);
            String mammalOrNot = "non-mammal";
            if (list[i].isMammal()){
                mammalOrNot = "mammal";
            }
            System.out.println(name + " is a " + mammalOrNot);
            int totalLegs = list[i].legCount();
            System.out.println("Did I forgot to tell you I have " + totalLegs +" legs.");
        }
        for (int i = 0 ; i<list.length;i++){
            if(list[i] instanceof IFamous ){
                String famousName = ((IFamous)list[i]).famous();
                System.out.println("This is a famous name of my animal type: " + famousName );
            }
        }
        // TODO Loop through all the pets in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.
    }

    public static void main(String[] args) {
        new AnimalApp().start();
    }
}
