package ictgradschool.industry.abstraction.pets;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal , IFamous {
    @Override
    public String sayHello() {
        String horseGreeting = "neigh";
        return horseGreeting;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        String myNameIs ="Mr. Ed the horse";
        return myNameIs;
    }

    @Override
    public int legCount() {
        int horseLegs = 4;
        return horseLegs;
    }

    @Override
    public String famous(){
        String famousName = "PharLap";
        return famousName;
    }
}

